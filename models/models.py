# -*- coding: utf-8 -*-

from odoo import models, fields, api

class Vehicle(models.Model):
    _name = 'transportation.vehicle'

    name = fields.Char(string="Nomor Kendaraan")
    vehicle_type = fields.Selection([('bus', 'Bis'),('taxi', 'Taksi')], string="Jenis Kendaraan")
    brand = fields.Char(string="Merk Kendaraan")
    trip = fields.One2many('transportation.trip', 'vehicle', string="Perjalanan")

class City(models.Model):
	_name = 'transportation.city'

	name = fields.Char(string="Nama Kota")

class Trip(models.Model):
    _name = 'transportation.trip'
    _order = 'origin_city asc'

    name = fields.Char(string="Kode Perjalanan", readonly=1, default="Trip")
    origin_city = fields.Many2one('transportation.city', string="Kota Asal")
    destination_city = fields.Many2one('transportation.city', string="Kota Tujuan")
    product_id = fields.Many2one("product.template", "Product", default=lambda self:self.env['product.template'].search([('name','=','Tiket')]).id)
    trip_line_ids = fields.One2many('transportation.trip.line', 'trip_id')
    vehicle = fields.Many2one('transportation.vehicle', string="Kendaraan")
    is_driver = fields.Many2one('res.partner', string="Supir")
    is_invoiced = fields.Boolean("Invoiced")
    departure_date = fields.Date("Departure Date", help="Departure date of the trip")
    price = fields.Float("Price", related="product_id.list_price")
    service_price = fields.Float("Service Price", compute="_compute_price")
    state = fields.Selection([('draft','Draft'),('queue','On Going'),('done','Done'),('cancel','Cancelled')], default='draft', required=True)
    item_line_ids = fields.One2many('transportation.item.line','trip_line_id')
    location_id = fields.Many2one('stock.picking')
    company_id = fields.Many2one("res.company", "Company", default=lambda self:self.env.user.company_id.id)
    currency_id = fields.Many2one("res.currency", "Currency", related="company_id.currency_id")
    capacity = fields.Integer("Capacity")
    passenger_count = fields.Integer("Passenger", compute="_compute_capacity")
    capacity_left = fields.Float("Capacity Left", compute="_compute_capacity")
    
    @api.depends("capacity", "item_line_ids")
    def _compute_capacity(self):
        passenger_count = len(self.trip_line_ids)
        self.passenger_count = passenger_count
        if self.capacity != 0:
            self.capacity_left = ((float(self.capacity)-float(passenger_count))/float(self.capacity))*100 
    
    @api.multi
    def action_confirm(self):
    	for trip in self:
            trip.state = "done"
            trip.action_create_invoice()
    
    @api.multi
    def action_process(self):
        self.state = "queue"
    
    @api.multi
    def action_cancel(self):
        self.state = "cancel"
    
    # @api.onchange("product_id")
    # def onchange_product_id(self):
    #     self.price = self.product_id.list_price

    @api.depends("price")
    def _compute_price(self):
        self.service_price = self.product_id.list_price * 0.1

    @api.multi
    def action_create_invoice(self):
    	# Definisikan Class
    	AccountInvoice = self.env['account.invoice']
    	self.is_invoiced = True
        # import ipdb;ipdb.set_trace()
        account_id = self.env['account.account'].search([('name','ilike','%Sales')])

    	# Loop per Line
    	for line in self.trip_line_ids:
        	invoice = AccountInvoice.create({
        		'partner_id' : line.partner_id.id,
        		'invoice_line_ids': [
                    (0, 0, {
        				"name" : self.name,
        				"quantity" : 1,
        				"price_unit" : self.price,
        				"account_id" : account_id.id,
                    }),
                    (0, 0, {
                        "name" : "Service",
                        "quantity" : 1,
                        "price_unit" : self.price * 0.1,
                        "account_id" : account_id.id,
                    })
                ]	
        	})            
                ## validate invoice
                invoice.action_invoice_open()
                line.invoice_id = invoice

    @api.model
    def create(self, vals):
        vals.update({'name':self.env['ir.sequence'].next_by_code("trip.sequence")})
        res = super(Trip, self).create(vals)
        return res


    @api.multi
    def action_create_picking(self):
        # Definisikan Class
        StockPicking = self.env['stock.picking']
        # self.is_invoiced = True
        # import ipdb;ipdb.set_trace()
        # account_id = self.env['account.account'].search([('name','ilike','%Sales')])

        # Loop per Line
        for line in self.item_line_ids:
            picking = StockPicking.create({
                'partner_id' : self.is_driver.id,
                'location_id' : 1,
                'picking_type_id' : 1,
                'location_dest_id' : 1,
                'move_lines': [
                    (0, 0, {
                        "name" : "test",
                        "product_id" : line.product_id.id,
                        "product_uom_qty" : line.qty,
                        "product_uom" : line.product_id.uom_id.id,

                    })
                ]
            })


class Tripline(models.Model):
    _name = 'transportation.trip.line'
    
    partner_id = fields.Many2one('res.partner', string = "Penumpang", required=1)
    seat_no = fields.Integer(string = "Nomor Tempat Duduk")
    payment_state = fields.Selection([('process','Process'),('done', 'Done')],'Payment Status', compute="_compute_payment")
    trip_id = fields.Many2one('transportation.trip', 'ID Trip')
    invoice_id = fields.Many2one('account.invoice', 'Invoice')
    sale_id = fields.Many2one('sale.order', 'Sale Order')
    sequence = fields.Integer("Sequence")
    
    @api.depends("invoice_id")
    def _compute_payment(self):
        for trip_line in self:
            if trip_line.invoice_id.state != "paid":
                trip_line.payment_state = 'process'
            else:
                trip_line.payment_state = 'done'


class Itemline(models.Model):
    _name = 'transportation.item.line'

    product_id = fields.Many2one('product.product', "Product")
    qty = fields.Char("Quantity")
    # state = fields.Char('State',readonly=True)
    trip_line_id = fields.Many2one('transportation.trip')
    picking_id = fields.Many2one('stock.picking', 'Picking')
    state = fields.Selection([('process','Process'),('done', 'Done')],'Status', compute="_compute_picking")

    @api.depends("picking_id")
    def _compute_picking(self):
        for item_line in self:
            if item_line.picking_id.state != "done":
                item_line.state = 'process'
            else:
                item_line.state = 'done'
