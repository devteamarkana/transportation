# -*- coding: utf-8 -*-
{
    'name': "Transportation",

    'summary': """
        Modul mengenai pengelolaan Transportasi""",

    'description': """
        Long description of module's purpose
    """,

    'author': "Gharta",
    'website': "http://www.yourcompany.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'account', 'sale'],

    # always loaded
    'data': [
        'security.xml',
        'views/views.xml',
        'sequence.xml',
    ],
}